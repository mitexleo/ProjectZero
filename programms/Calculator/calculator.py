#Define add(ition), sub(straction), div(ision), mul(tiplication) function
def add(a,b):
    answer = a+b
    print(str(a) + " +" + str(b) + " = " + str(answer))

def sub(a,b):
    answer = a-b
    print(str(a) + " -" + str(b) + " = " + str(answer))

def div(a,b):
    answer = a/b
    print(str(a) + " ÷" + str(b) + " = " + str(answer))

def mul(a,b):
    answer = a*b
    print(str(a) + " x" + str(b) + " = " + str(answer))

#Guide
name = input("What's your name ?: ")
print("Hello, ", name , "!")
print("Welcome to our simple calculator. Here you can do only simple task at this moment. We're working to improve this program.")
print("Here's the details of the options:")

print("A = Addition")
print("S = Substraction")
print("M = Multiplication")
print("D = Division")
print("E = Exit")

#Choice
while True:
    choicelist = ["a","s","m","d","e"]

    choice = input("What do you want to do ?: ").lower()

    if choice not in choicelist:
        print("Please enter a valid option")

    if choice == "a":
        print("Addition")
        x = int(input("Input First Value: "))
        y = int(input("Input Second Value: "))
        add(x,y)
    
    elif choice == "s":
        print("Substraction")
        x = int(input("Input First Value: "))
        y = int(input("Input Second Value: "))
        sub(x,y)
    elif choice == "m":
        print("Multiplication")
        x = int(input("Input First Value: "))
        y = int(input("Input Second Value: "))
        mul(x,y)
    elif choice == "d":
        print("Division")
        x = int(input("Input First Value: "))
        y = int(input("Input Second Value: "))
        div(x,y)
    elif choice == "e":
        print("Goodbye", name)
        exit()
