#This is a simple program for finding the distance between two points
# Import Math module
import math

def distance(x1,y1,x2,y2):
    dx = x2 - x1
    dy = y2 - y1
    dsquared = dx**2 + dy**2
    result = math.sqrt(dsquared)
    return result
#Intro
print("This is a simple program for finding the distance between two points. Enter the values of x1,y1 and x2,y2.")
x1 = int(input("Enter the value of x1: "))
y1 = int(input("Enter the value of y1: "))
x2 = int(input("Enter the value of x2: "))
y2 = int(input("Enter the value of y2: "))

#Convert the values to Integer
#a = int(x1)
#b = int(y1)
#c = int(x2)
#d = int(y2)

distance_result = distance(x1,y1,x2,y2)
print("The distance between the two point is: ", distance_result)

