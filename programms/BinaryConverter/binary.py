while True:
    x = input("What do you want to convert to Binary (Type q to Quit) ?: ")
    if x == "q":
        quit()
    else :
        if x.isdigit():
            y = int(x)
            z = bin(y)
            print(z)
        else :
            print("Please enter a number")
            continue
    continue
