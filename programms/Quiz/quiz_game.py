#Welocome Users
print ("Welcome to the Game !")

#Ask for consent
playing = input("Do you want to play ? ")

if playing.lower() != "yes":
    quit()
print ("Okay! Let's Go :)")
#Score
score = 0
#Questions
#We"ll convert all the inputs to lower case
q1 = input("What does CPU stand for? ")
if q1.lower() == "central processing unit":
    print ("Correct!")
    score += 1
else:
    print ("You're Wrong!")

q2 = input("What does GPU stand for? ")
if q2.lower() == "graphics processing unit":
    print ("Correct!")
    score += 1
else:
    print ("Incorrect!")

q3 = input("What does RAM stand for? ")
if q3.lower() == ("random access memory"):
    print ("Correct!")
    score += 1
else:
    print ("Wrong")

#Show Result
print ("You Got " + str(score) + " Questions Correct!")

#Show the result in percentage
print (str((score / 3) * 100) + " % of your answers are correct")
