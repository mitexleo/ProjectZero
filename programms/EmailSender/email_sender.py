#Import Email Message Module
#It should be preinstalled in your computer
from email.message import EmailMessage
import ssl
import smtplib
from email_cred import pwd
from email_cred import email_from

email_sender = email_from
email_pwd = pwd

consent = input("Do you wanna send email? Type Y to continue or Q to quit: ").lower()

if consent == "q":
    exit()
email_rec = input("Enter Recipient Address: ")

subject = input("Enter the subject: ")
body = input("Enter the message: ")

em = EmailMessage()
em["From"] = email_sender
em["To"] = email_rec
em["subject"] = subject
em.set_content(body)

context = ssl.create_default_context()

with smtplib.SMTP_SSL("smtp.gmail.com", 465, context = context) as smtp:
    smtp.login(email_sender, email_pwd)
    smtp.sendmail(email_sender, email_rec, em.as_string())
