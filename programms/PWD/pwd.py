#This project is abandoned
#Import Cryptography Module
from cryptography.fernet import Fernet


#Define key
def write_key():
    key =Fernet.generate_key()
    with open("key.key", "wb") as key_file:
    key_file.write(key)

#Load the key
def load_key():
    file = open("key.key", "rb")
    key = file.read()
    file.close()
    return key
#Ask for mastar pwd & Load the key
master_pwd = input("Enter the master password: ")
key = load_key() + master_pwd.encode()
fer = Fernet(key)


#Functions
def view():
    with open("passwords.txt","a") as f:
        for line in f.readlines():
            data = line.rstrip()
            user, passw = data.spliy("|")
            print("User:", user, "| Password:", fer.decrypt(passw.encode()).decode())


def add():
    name = input("Account Name: ")
    pwd = input("Password: ")

    with open("passwords.txt","a") as f:
        f.write(name + "|" + fer.encrypt(pwd.encode()).decode() + "\n")


#Ask users which mode they wanna go
while True:
    mode = input("Would you like add a new password or view existing ones (view, add)? (Press Q to quit) ").lower()

    if mode == "q":
        break

    if mode == "view":
        view()
    elif mode == "add":
        add()
    else:
        print ("Invalid mode.")
        continue 
