import math 

# Input coefficients 

a = int(input('a: ')) 

b = int(input('b: ')) 

c = int(input('c: ')) 

# Calculate discriminant 

discriminant = math.pow(b, 2) - (4 * a * c) 

# Calculate solution 

if discriminant > 0: 

    x1 = (-b + math.sqrt(discriminant)) / (2 * a) 

    x2 = (-b - math.sqrt(discriminant)) / (2 * a) 

    print('x1 =', x1) 

    print('x2 =', x2) 

else: 

    print('The equation has no real solution.')
