#Rounding Numbers
pi = 3.1214
two_dec= round(pi,2)
print(two_dec)

#Raising a Number two a power using pow() Function

x = pow (2,2)
y = pow (3,2)
z = pow (5,10)

print (x)
print (y)
print (z)

#Getting Absolute Value
a = abs(-475568)
b = abs(686)
print (a)
print (b)
