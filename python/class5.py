class Student:
    def __init__(self,name, age):
        self.name = name
        self.age = age

    def greeting(self, greetings):
        print("Hello", self.name , greetings)

st1 = Student("John Doe", "35")

st1.greeting("How are you ?")

#Inheritence

class Animal:
    def __init__(self, name):
        self.name = name

    def walk(self):
        print(self.name + " Walks")


a = Animal("Dog")
a.walk()

b = Animal("Cat")
b.walk()
