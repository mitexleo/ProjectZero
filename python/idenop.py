#The is Operator
# The is operator returns true if the both operands points to a same object 
x = [1,2,3,4,5,6]
y = [1,2,3,4,5,6]
z = y
print (x is y) #It will return false because both operands are not pointing to the same object. Though the values of both lists are same 

print (y is z) #True
