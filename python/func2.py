#Multiple Arg
def sum1(num1,num2):
    sum = num1+num2
    print (sum)

#Call
sum1(2,3)

#Return Argument 
def sum2(num1,num2,num3):
    sum = num1 + num2 * num3
    return sum
print(sum2(3,5,6))
